<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

//MODELO
use App\Models\Persona;

class PersonaController extends Controller
{
    //
    public function home(){
        $hombres = Persona::ListadoHombres();
        $mujeres = Persona::ListadoMujeres();
        $personas = Persona::Listado();
        
        return view('home',['hombres'=>$hombres,'mujeres'=>$mujeres,'personas'=>$personas]);
    }
    
    public function inicio(){
        $personas = Persona::Listado();
        return view('personas.listado',['personas'=>$personas]);
    }
    
    public function listadoJson(){
        $datos=Persona::JSON($request->all());
        
        return response($datos);
    }
    
    public function editar($id){
        $persona = Persona::getDatos($id);
        //dd($persona);
        $titulo = ($id==0?'Agregar Persona':('Editar Persona: '.(isset($persona[0]['nombre'])?$persona[0]['nombre']:'')));
        
        return view('personas.editar',['persona'=>$persona,'titulo'=>$titulo,'id'=>$id]);
    }
    
    public function Guardar(Request $request){
        $id = $request->input('id');
        $nombre=$request->input('nombre');
        $apellido_paterno=$request->input('apellido_paterno');
        $apellido_materno=$request->input('apellido_materno');
        $sexo=$request->input('sexo');
        $edad=$request->input('edad');
        $fecha_nacimiento=$request->input('fecha_nacimiento');
        $estado_civil=$request->input('estado_civil');
        $numero_telefonico=$request->input('numero_telefonico');        
        $correo=$request->input('correo');
        $pais=$request->input('pais');
        $estado=$request->input('estado');
        $ciudad=$request->input('ciudad');
        $estatus_id=$request->input('estatus_id');
        
        if($id==0){
           $persona = new Persona();
        }else{
           $persona = Persona::where('id',$id)->first();
        }
        
        $persona->nombre=$nombre;
        $persona->apellido_paterno=$apellido_paterno;
        $persona->apellido_materno=$apellido_materno;
        $persona->sexo=isset($sexo)?$sexo:1;
        $persona->edad=isset($edad)?$edad:0;
        $persona->fecha_nacimiento=$fecha_nacimiento;
        $persona->estado_civil=isset($estado_civil)?$estado_civil:1;
        $persona->numero_telefonico=$numero_telefonico;
        $persona->correo=$correo;
        $persona->pais=$pais;
        $persona->estado=$estado;
        $persona->ciudad=$ciudad;
        $persona->estatus_id=isset($estatus_id)?$estatus_id:1;
        $persona->save();
        
        return $persona;
    }
    
    public function eliminar(Request $request){
        $id=$request->input('id');
        $datos = Persona::where('id', $id)->delete();
        
        return response($datos);
    }
}
