<?php

Route::get('/personas/listado','App\Http\Controllers\PersonaController@inicio');
Route::post('/personas/listado.json','App\Http\Controllers\PersonaController@listadoJson');

Route::get('/personas/editar/{id}','App\Http\Controllers\PersonaController@editar');

Route::post('/personas/eliminar','App\Http\Controllers\PersonaController@eliminar');

Route::post('/personas/guardar','App\Http\Controllers\PersonaController@guardar');
