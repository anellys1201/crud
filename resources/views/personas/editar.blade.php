<!DOCTYPE html>
<html >
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CRUD</title>
  <base target="_self">
  <meta name="description" content="CRUD DE PERSONAS" />
  <meta name="google" value="notranslate">

  
  <!--stylesheets / link tags loaded here-->



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  

  

  <style type="text/css">body, html {
  height:100%;
}

/*
 * Off Canvas sidebar at medium breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 992px) {

  .row-offcanvas {
    position: relative;
    -webkit-transition: all 0.25s ease-out;
    -moz-transition: all 0.25s ease-out;
    transition: all 0.25s ease-out;
  }

  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -33%;
  }

  .row-offcanvas-left.active {
    left: 33%;
    margin-left: -6px;
  }

  .sidebar-offcanvas {
    position: absolute;
    top: 0;
    width: 33%;
    height: 100%;
  }
}

/*
 * Off Canvas wider at sm breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 34em) {
  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -45%;
  }

  .row-offcanvas-left.active {
    left: 45%;
    margin-left: -6px;
  }
  
  .sidebar-offcanvas {
    width: 45%;
  }
}

</style>

</head>
<body >
  
  
<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3">
    <div class="flex-row d-flex">
        <button type="button" class="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{asset('/')}}" title="Free Bootstrap 4 Admin Template">ADMINISTRADOR DE PERSONAS</a>
    </div>
</nav>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-md-3 col-lg-2 sidebar-offcanvas bg-light pl-0" id="sidebar" role="navigation">
            <ul class="nav flex-column sticky-top pl-0 pt-5 mt-3">
                <li class="nav-item"><a class="nav-link" href="{{asset('/')}}">Inicio</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Personas ▾</a>
                    <ul class="list-unstyled flex-column pl-3 collapse" id="submenu1" aria-expanded="false">
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/listado')}}">Ver Listado</a></li>
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/0')}}">Agregar Registro</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/col-->

        <div class="col main pt-5 mt-3">
            <p class="lead mt-5">
                Edición de Personas
            </p>
            <div class="row my-4">
                <div class="col-lg-10 col-md-8">
                    <form name="form" action="javascript:guardar()">
                        <div class="form-row">
                            <div class="form-group col-md-4">
                                <label for="nombre">Nombre</label>
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre(s)" value="{{isset($persona[0]['nombre'])?$persona[0]['nombre']:''}}">
                            </div>
                                
                            <div class="form-group col-md-4">
                                <label for="apellido_paterno">Apellido Paterno</label>
                                <input type="text" class="form-control" id="apellido_paterno" name="apellido_paterno" placeholder="Apellido Paterno" value="{{isset($persona[0]['apellido_paterno'])?$persona[0]['apellido_paterno']:''}}">
                            </div>
                                
                            <div class="form-group col-md-4">
                                <label for="apellido_paterno">Apellido Materno</label>
                                <input type="text" class="form-control" id="apellido_materno" name="apellido_materno" placeholder="Apellido Materno" value="{{isset($persona[0]['apellido_materno'])?$persona[0]['apellido_materno']:''}}">
                            </div>
                                
                            <div class="form-group col-md-3">
                                <label for="sexo">Sexo</label>
                                <select id="sexo" name="sexo" class="form-control">
                                    <option value="1" {{(isset($persona[0]['sexo']) && $persona[0]['sexo']==1)?'Selected="selected"':''}}>Masculino</option>
                                    <option value="2" {{(isset($persona[0]['sexo']) && $persona[0]['sexo']==2)?'Selected="selected"':''}}>Femenino</option>
                                </select>
                            </div>
                                
                            <div class="form-group col-md-3">
                                <label for="edad">Edad (años)</label>
                                <input type="number" class="form-control" id="edad" name="edad" placeholder="Edad" value="{{isset($persona[0]['edad'])?$persona[0]['edad']:''}}">
                            </div>
                                
                            <div class="form-group col-md-3">
                                <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                <input type="date" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="Fecha de nacimiento" value="{{isset($persona[0]['fecha_nacimiento'])?$persona[0]['fecha_nacimiento']:''}}"> 
                            </div>
                                
                            <div class="form-group col-md-3">
                                <label for="estado_civil">Estado Civil</label>
                                <select id="estado_civil" name="estado_civil" class="form-control">
                                    <option value="1" {{(isset($persona[0]['estado_civil']) && $persona[0]['estado_civil']==1)?'Selected="selected"':''}}>Soltero</option>
                                    <option value="2" {{(isset($persona[0]['estado_civil']) && $persona[0]['estado_civil']==2)?'Selected="selected"':''}}>Casado</option>
                                    <option value="3" {{(isset($persona[0]['estado_civil']) && $persona[0]['estado_civil']==3)?'Selected="selected"':''}}>Divorciado</option>
                                    <option value="4" {{(isset($persona[0]['estado_civil']) && $persona[0]['estado_civil']==4)?'Selected="selected"':''}}>Union libre</option>
                                </select>
                            </div>
                        
                            <div class="form-group col-md-6">
                              <label for="numero_telefonico">Numero Telefonico</label>
                              <input type="text" class="form-control" id="numero_telefonico" name="numero_telefonico" placeholder="Numero Telefonico" value="{{isset($persona[0]['numero_telefonico'])?$persona[0]['numero_telefonico']:''}}">
                            </div>
                                
                            <div class="form-group col-md-6">
                              <label for="correo">Correo</label>
                              <input type="email" class="form-control" id="correo" name="correo" placeholder="Correo" value="{{isset($persona[0]['correo'])?$persona[0]['correo']:''}}">
                            </div>
                                
                            <div class="form-group col-md-4">
                              <label for="pais">País</label>
                              <input type="text" class="form-control" id="pais" name="pais" placeholder="País" value="{{isset($persona[0]['pais'])?$persona[0]['pais']:''}}">
                            </div>
                                
                            <div class="form-group col-md-4">
                              <label for="estado">Estado</label>
                              <input type="text" class="form-control" id="estado" name="estado" placeholder="Estado" value="{{isset($persona[0]['estado'])?$persona[0]['estado']:''}}">
                            </div>
                                
                            <div class="form-group col-md-4">
                              <label for="ciudad">Ciudad</label>
                              <input type="text" class="form-control" id="ciudad" name="ciudad" placeholder="Ciudad" value="{{isset($persona[0]['ciudad'])?$persona[0]['ciudad']:''}}">
                            </div>
                        </div>
                        
                        
                            
                        <button type="submit" class="btn btn-primary">Guardar</button>
                      </form>
                </div>
            </div>
            <!--/row-->

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<footer class="container-fluid">
    <p class="text-right small">©2021 Anellys</p>
</footer>
  
  <script>
    // sandbox disable popups
    if (window.self !== window.top && window.name!="view1") {;
      window.alert = function(){/*disable alert*/};
      window.confirm = function(){/*disable confirm*/};
      window.prompt = function(){/*disable prompt*/};
      window.open = function(){/*disable open*/};
    }
    
    // prevent href=# click jump
    document.addEventListener("DOMContentLoaded", function() {
      var links = document.getElementsByTagName("A");
      for(var i=0; i < links.length; i++) {
        if(links[i].href.indexOf('#')!=-1) {
          links[i].addEventListener("click", function(e) {
          console.debug("prevent href=# click");
              if (this.hash) {
                if (this.hash=="#") {
                  e.preventDefault();
                  return false;
                }
                else {
                  /* this breaks routing */
                  var el = document.getElementById(this.hash.replace(/#/, ""));
                  if (el) {
                    el.scrollIntoView(true);
                  }
                  
                }
              }
              return false;
          })
        }
      }
    }, false);
  </script>
  
  <!--scripts loaded here-->
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
  
  <script>
  $(document).ready(function() {
    
  $('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
  });
  
});
  </script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        function guardar(){
            var data = new FormData();
            data.append('_token','{{csrf_token()}}');
            data.append('id',{{$id}});
            data.append('nombre', $('#nombre').val());
            data.append('apellido_paterno',$('#apellido_paterno').val());
            data.append('apellido_materno',$('#apellido_materno').val());
            data.append('sexo',$('#sexo').val());
            data.append('edad',$('#edad').val());
            data.append('fecha_nacimiento',$('#fecha_nacimiento').val());
            data.append('estado_civil',$('#estado_civil').val());
            data.append('numero_telefonico',$('#numero_telefonico').val());
            data.append('correo',$('#correo').val());
            data.append('pais',$('#pais').val());
            data.append('estado',$('#estado').val());
            data.append('ciudad',$('#ciudad').val());
            data.append('estatus_id',1);
            
            var url="{{asset('/personas/guardar')}}";
            $.ajax({
                url:url,
                type:'POST',
                contentType:false,
                data:data,
                processData:false,
                cache:false,
                success:function(data,textStatus,jqXHR)
                {
                    

                    swal("Guardado", "El registro se guardo con exito", "success").then((value)=>{
                        location.replace("{{asset('/personas/listado')}}");
                    });
                },
                error:function(){
                    swal('ERROR','Vuelve a intentar guardar','error')
                }
            });
        }
        
    </script>

</body>
</html>

