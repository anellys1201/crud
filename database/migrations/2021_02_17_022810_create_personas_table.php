<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personas', function (Blueprint $table) {
            $table->id();
            $table->text('nombre')->nullable;
            $table->text('apellido_paterno')->nullable;
            $table->text('apellido_materno')->nullable;
            $table->integer('sexo')->default(1);
            $table->integer('edad')->nullable;
            $table->date('fecha_nacimiento')->nullable;
            $table->integer('estado_civil')->default(1);
            $table->text('numero_telefonico')->nullable;
            $table->text('correo')->nullable;
            $table->text('pais')->nullable;
            $table->text('estado')->nullable;
            $table->text('ciudad')->nullable;
            $table->integer('estatus_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personas');
    }
}
