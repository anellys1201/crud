<!DOCTYPE html>
<html >
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CRUD</title>
  <base target="_self">
  <meta name="description" content="CRUD DE PERSONAS" />
  <meta name="google" value="notranslate">

  
  <!--stylesheets / link tags loaded here-->



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  

  

  <style type="text/css">body, html {
  height:100%;
}

/*
 * Off Canvas sidebar at medium breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 992px) {

  .row-offcanvas {
    position: relative;
    -webkit-transition: all 0.25s ease-out;
    -moz-transition: all 0.25s ease-out;
    transition: all 0.25s ease-out;
  }

  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -33%;
  }

  .row-offcanvas-left.active {
    left: 33%;
    margin-left: -6px;
  }

  .sidebar-offcanvas {
    position: absolute;
    top: 0;
    width: 33%;
    height: 100%;
  }
}

/*
 * Off Canvas wider at sm breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 34em) {
  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -45%;
  }

  .row-offcanvas-left.active {
    left: 45%;
    margin-left: -6px;
  }
  
  .sidebar-offcanvas {
    width: 45%;
  }
}

.card {
    overflow:hidden;
}

.card-body .rotate {
    z-index: 8;
    float: right;
    height: 100%;
}

.card-body .rotate i {
    color: rgba(20, 20, 20, 0.15);
    position: absolute;
    left: 0;
    left: auto;
    right: -10px;
    bottom: 0;
    display: block;
    -webkit-transform: rotate(-44deg);
    -moz-transform: rotate(-44deg);
    -o-transform: rotate(-44deg);
    -ms-transform: rotate(-44deg);
    transform: rotate(-44deg);
}
</style>

</head>
<body >
  
  
<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3">
    <div class="flex-row d-flex">
        <button type="button" class="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{asset('/')}}" title="Free Bootstrap 4 Admin Template">ADMINISTRADOR DE PERSONAS</a>
    </div>
</nav>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-md-3 col-lg-2 sidebar-offcanvas bg-light pl-0" id="sidebar" role="navigation">
            <ul class="nav flex-column sticky-top pl-0 pt-5 mt-3">
                <li class="nav-item"><a class="nav-link" href="{{asset('/')}}">Inicio</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Personas ▾</a>
                    <ul class="list-unstyled flex-column pl-3 collapse" id="submenu1" aria-expanded="false">
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/listado')}}">Ver Listado</a></li>
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/editar/0')}}">Agregar Registro</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/col-->

        <div class="col main pt-5 mt-3">
            <h1 class="display-4 d-none d-sm-block">
            Bienvenid@
            </h1>
            <p class="lead d-none d-sm-block">Este es un ejemplo CRUD en Laravel</p>

            <div class="row mb-3">
                <div class="col-xl-4 col-sm-12 py-2">
                    <div class="card bg-success text-white h-100">
                        <div class="card-body bg-success">
                            <div class="rotate">
                                <i class="fa fa-user fa-4x"></i>
                            </div>
                            <h6 class="text-uppercase">Personas (total)</h6>
                            <h1 class="display-4">{{sizeof($personas)}}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-12 py-2">
                    <div class="card text-white bg-danger h-100">
                        <div class="card-body bg-danger">
                            <div class="rotate">
                                <i class="fa fa-user fa-4x"></i>
                            </div>
                            <h6 class="text-uppercase">Personas (Mujeres)</h6>
                            <h1 class="display-4">{{sizeof($mujeres)}}</h1>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-sm-12 py-2">
                    <div class="card text-white bg-info h-100">
                        <div class="card-body bg-info">
                            <div class="rotate">
                                <i class="fa fa-user fa-4x"></i>
                            </div>
                            <h6 class="text-uppercase">Personas (Hombres)</h6>
                            <h1 class="display-4">{{sizeof($hombres)}}</h1>
                        </div>
                    </div>
                </div>
            </div>
            <!--/row-->

            <a id="features"></a>
            <hr>
            <p class="lead mt-5">
                Personas dadas de alta en el sistema
            </p>
            <div class="row my-4">
                <div class="col-lg-1"></div>
                <div class="col-lg-10 col-md-8">
                    <div class="table-responsive">
                        <table class="table table-striped" id="tabla">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Apellido Materno</th>
                                    <th>Apellido Paterno</th>
                                    <th>Sexo</th>
                                    <th>Edad</th>
                                    <th>Pais</th>
                                    <th>Numero tel</th>
                                    <th>Correo</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @if(sizeof($personas)>0)
                                    @for($i=0; $i < sizeof($personas); $i++)
                                        <tr>
                                            <td>{{($i+1)}}</td>
                                            <td>{{isset($personas[$i]['nombre'])?$personas[$i]['nombre']:''}}</td>
                                            <td>{{isset($personas[$i]['apellido_paterno'])?$personas[$i]['apellido_paterno']:''}}</td>
                                            <td>{{isset($personas[$i]['apellido_materno'])?$personas[$i]['apellido_materno']:''}}</td>
                                            <td>{{(isset($personas[$i]['sexo']) && $personas[$i]['sexo'] == 1)?'Masculino':'Femenino'}}</td>
                                            <td>{{isset($personas[$i]['edad'])?$personas[$i]['edad']:''}}</td>
                                            <td>{{isset($personas[$i]['pais'])?$personas[$i]['pais']:''}}</td>
                                            <td>{{isset($personas[$i]['numero_telefonico'])?$personas[$i]['numero_telefonico']:''}}</td>
                                            <td>{{isset($personas[$i]['correo'])?$personas[$i]['correo']:''}}</td>
                                        </tr>
                                    @endfor
                                @else
                                    <p>No hay ningun registro en la tabla</p>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--/row-->

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<footer class="container-fluid">
    <p class="text-right small">©2021 Anellys</p>
</footer>
  
  <script>
    // sandbox disable popups
    if (window.self !== window.top && window.name!="view1") {;
      window.alert = function(){/*disable alert*/};
      window.confirm = function(){/*disable confirm*/};
      window.prompt = function(){/*disable prompt*/};
      window.open = function(){/*disable open*/};
    }
    
    // prevent href=# click jump
    document.addEventListener("DOMContentLoaded", function() {
      var links = document.getElementsByTagName("A");
      for(var i=0; i < links.length; i++) {
        if(links[i].href.indexOf('#')!=-1) {
          links[i].addEventListener("click", function(e) {
          console.debug("prevent href=# click");
              if (this.hash) {
                if (this.hash=="#") {
                  e.preventDefault();
                  return false;
                }
                else {
                  /* this breaks routing */
                  var el = document.getElementById(this.hash.replace(/#/, ""));
                  if (el) {
                    el.scrollIntoView(true);
                  }
                  
                }
              }
              return false;
          })
        }
      }
    }, false);
  </script>
  
  <!--scripts loaded here-->
  
  <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
  
  
  <script>
  $(document).ready(function() {
    
  $('[data-toggle=offcanvas]').click(function() {
    $('.row-offcanvas').toggleClass('active');
  });
  
});
  </script>

</body>
</html>

