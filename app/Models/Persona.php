<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $fillable = ['id', 'nombre', 'apellido_paterno', 'apellido_materno', 'sexo', 'edad', 'fecha_nacimiento', 'estado_civil',
                           'numero_telefonico','correo','pais','estado','ciudad','estatus_id'];
    
    public function scopeListado($query){
        $datos = $query->select('*')->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array();
        return $datos;
    }
    
    public function scopeJSON($query,$datos){
        $resultados = $query->
        select('id','nombre','apellido_paterno','apellido_materno','sexo','edad','pais','numero_telefonico','correo')->
        where('nombre','ILIKE','%'.(isset($datos['search']['value'])?$datos['search']['value']:null).'%')->
        orderBy('id', 'desc')->
        limit($datos['length'])->offset($datos['start'])->get()->toArray();
        $totales=Persona::select(\DB::raw('count(id) as total'))
        ->where('nombre','ILIKE','%'.(isset($datos['search']['value'])?$datos['search']['value']:null).'%')
        ->get()->toArray();
        
        return ['data'=>(sizeof($resultados) > 0)?$resultados:array(),
				'recordsTotal'=>(isset($totales[0]['total']))?$totales[0]['total']:0,
				'search' => (isset($datos['search']['value'])?$datos['search']['value']:''),
				'draw' =>(int)isset($datos['draw'])?$datos['draw']:0,
				'recordsFiltered' => (isset($totales[0]['total']))?$totales[0]['total']:0
			    ];
    }
    
    public function scopeListadoHombres($query){
        $datos = $query->select('*')
        ->where('sexo',1)->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array();
        return $datos;
    }
    
    public function scopeListadoMujeres($query){
        $datos = $query->select('*')
        ->where('sexo',2)->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array();
        return $datos;
    }
    
    public function scopeGetDatos($query,$id){
        $datos = $query->select('*')
        ->where('id',$id)->get()->toArray();
        $datos=(sizeof($datos) > 0)?$datos:array();
        return $datos;
    }
}
