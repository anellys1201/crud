<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>CRUD</title>
  <base target="_self">
  <meta name="description" content="CRUD DE PERSONAS" />
  <meta name="google" value="notranslate">

  
  <!--stylesheets / link tags loaded here-->



  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" />
  <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />
  

  

  <style type="text/css">body, html {
  height:100%;
}

/*
 * Off Canvas sidebar at medium breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 992px) {

  .row-offcanvas {
    position: relative;
    -webkit-transition: all 0.25s ease-out;
    -moz-transition: all 0.25s ease-out;
    transition: all 0.25s ease-out;
  }

  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -33%;
  }

  .row-offcanvas-left.active {
    left: 33%;
    margin-left: -6px;
  }

  .sidebar-offcanvas {
    position: absolute;
    top: 0;
    width: 33%;
    height: 100%;
  }
}

/*
 * Off Canvas wider at sm breakpoint
 * --------------------------------------------------
 */
@media screen and (max-width: 34em) {
  .row-offcanvas-left
  .sidebar-offcanvas {
    left: -45%;
  }

  .row-offcanvas-left.active {
    left: 45%;
    margin-left: -6px;
  }
  
  .sidebar-offcanvas {
    width: 45%;
  }
}

</style>

</head>
<body >
  
  
<nav class="navbar fixed-top navbar-expand-md navbar-dark bg-primary mb-3">
    <div class="flex-row d-flex">
        <button type="button" class="navbar-toggler mr-2 " data-toggle="offcanvas" title="Toggle responsive left sidebar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <a class="navbar-brand" href="{{asset('/')}}" title="Free Bootstrap 4 Admin Template">ADMINISTRADOR DE PERSONAS</a>
    </div>
</nav>
<div class="container-fluid" id="main">
    <div class="row row-offcanvas row-offcanvas-left">
        <div class="col-md-3 col-lg-2 sidebar-offcanvas bg-light pl-0" id="sidebar" role="navigation">
            <ul class="nav flex-column sticky-top pl-0 pt-5 mt-3">
                <li class="nav-item"><a class="nav-link" href="{{asset('/')}}">Inicio</a></li>
                <li class="nav-item">
                    <a class="nav-link" href="#submenu1" data-toggle="collapse" data-target="#submenu1">Personas ▾</a>
                    <ul class="list-unstyled flex-column pl-3 collapse" id="submenu1" aria-expanded="false">
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/listado')}}">Ver Listado</a></li>
                       <li class="nav-item"><a class="nav-link" href="{{asset('/personas/0')}}">Agregar Registro</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <!--/col-->

        <div class="col main pt-5 mt-3">
            <p class="lead mt-5">
                Listado de Personas
            </p>
            <div class="row my-4">
                <div class="col-lg-1">
                    <a href="{{asset('personas/editar/0')}}" class="btn btn-primary">Agregar</a>
                </div>
                <div class="col-lg-10 col-md-8">
                    <div class="table-responsive">
                        <table class="table table-striped" id="tabla">
                            <thead class="thead-inverse">
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Apellido Materno</th>
                                    <th>Apellido Paterno</th>
                                    <th>Sexo</th>
                                    <th>Edad</th>
                                    <th>Pais</th>
                                    <th>Numero tel</th>
                                    <th>Correo</th>
                                    <th>Editar</th>
                                    <th>Eliminar</th>
                                    
                                </tr>
                            </thead>
                            <tbody>
                                @if(sizeof($personas)>0)
                                    @for($i=0; $i < sizeof($personas); $i++)
                                        <tr>
                                            <td>{{($i+1)}}</td>
                                            <td>{{isset($personas[$i]['nombre'])?$personas[$i]['nombre']:''}}</td>
                                            <td>{{isset($personas[$i]['apellido_paterno'])?$personas[$i]['apellido_paterno']:''}}</td>
                                            <td>{{isset($personas[$i]['apellido_materno'])?$personas[$i]['apellido_materno']:''}}</td>
                                            <td>{{(isset($personas[$i]['sexo']) && $personas[$i]['sexo'] == 1)?'Masculino':'Femenino'}}</td>
                                            <td>{{isset($personas[$i]['edad'])?$personas[$i]['edad']:''}}</td>
                                            <td>{{isset($personas[$i]['pais'])?$personas[$i]['pais']:''}}</td>
                                            <td>{{isset($personas[$i]['numero_telefonico'])?$personas[$i]['numero_telefonico']:''}}</td>
                                            <td>{{isset($personas[$i]['correo'])?$personas[$i]['correo']:''}}</td>
                                            <td><a href="{{asset('personas/editar/'.(isset($personas[$i]['id'])?$personas[$i]['id']:0))}}" class="btn btn-primary">Editar</a></td>
                                            <td><button onclick="eliminar({{isset($personas[$i]['id'])?$personas[$i]['id']:0}});" class="btn btn-danger">Eliminar</button></td>
                                        </tr>
                                    @endfor
                                @else
                                    <p>No hay ningun registro en la tabla</p>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!--/row-->

        </div>
        <!--/main col-->
    </div>

</div>
<!--/.container-->
<footer class="container-fluid">
    <p class="text-right small">©2021 Anellys</p>
</footer>
  
  <!--scripts loaded here-->
  
    <script src="//ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
    
    
        function eliminar(id){
        
            var data = new FormData();
            data.append('_token','{{ csrf_token() }}');
            data.append('id',id);
            var url="{{asset('personas/eliminar')}}";
            $.ajax({
                url:url,
                type:'POST',
                contentType:false,
                data:data,
                processData:false,cache:false,
                success: function(data, textStatus, jqXHR)
                {
                    swal('Eliminado','Registro eliminado','success');
                    location.reload();
                },
                error: function()
                {
                    swal('Error','Ocurrio un error, vuelve a intentarlo de nuevo','error');
                }
            });
            
        }
    </script>

</body>
</html>

